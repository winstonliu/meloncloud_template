from django.conf import settings
from django.contrib import admin
from django.conf.urls import patterns, url, include
from django.views.generic import TemplateView, RedirectView

from root_dir import root_dir

admin.autodiscover()

urlpatterns = patterns('main.views',
    (r'^$', 'index'),
    (r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns('',
    (r'^favicon\.ico$', RedirectView.as_view(url='/static/images/favicon.ico')),
    (r'^robots\.txt$', RedirectView.as_view(url='/static/robots.txt')),
)
